FROM python:3.7.3-alpine3.9

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY secrets/root-ca.cert root.cert
RUN cat root.cert >> /usr/local/lib/python3.7/site-packages/certifi/cacert.pem
RUN rm root.cert

COPY start.sh .
RUN chmod +x start.sh

COPY setup.py .
COPY pki ./pki
RUN pip install .

CMD ["./start.sh"]
