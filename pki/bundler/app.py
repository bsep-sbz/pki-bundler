import logging

from flask import Flask

from pki.bundler.resources import router


def create_app():
    app = Flask(__name__)
    configure(app)
    app.register_blueprint(router)
    return app


def configure(app):
    if __name__ != '__main__':
        gunicorn_logger = logging.getLogger('gunicorn.error')
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)


application = create_app()

