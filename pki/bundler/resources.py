from flask import (
    Blueprint,
    request
)
from pki.bundler.services import (
    bundle,
    validate
)

router = Blueprint('router', __name__)


@router.route('/bundle', methods=['POST'])
def bundler():
    if not request.is_json:
        return '', 400

    data = request.json
    if not validate(data):
        return '', 400

    return bundle(data)
