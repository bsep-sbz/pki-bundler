import re

import requests


URL_MATCHER = re.compile(
    r'^(?:http|ftp)s?://' # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
    r'localhost|' #localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
    r'(?::\d+)?' # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE
)


def validate(data):
    if not isinstance(data, dict):
        return False

    if 'certificates' not in data:
        return False

    if not isinstance(data['certificates'], dict):
        return False

    for key, values in data['certificates'].items():
        if not isinstance(key, str):
            return False

        if not URL_MATCHER.fullmatch(key):
            return False

        if not isinstance(values, list):
            return False

        for value in values:
            if not isinstance(value, str):
                return False

            if not value.isdigit():
                return False

    return True


def bundle(data):
    certs = []
    additional_opts = ''

    if data.get('bundle', False):
        additional_opts += '?bundle'

        if data.get('include_root', False):
            additional_opts += '&include_root'

    for key, values in data['certificates'].items():
        for serial_number in values:
            url = f'{key}/certificates/{serial_number}{additional_opts}'
            res = requests.get(url)
            certs.append(res.text)

    return ''.join(certs)
