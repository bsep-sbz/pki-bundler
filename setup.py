from setuptools import setup, find_packages

setup(
    name='MegaTravel PKI Bundler',
    version='1.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False
)
